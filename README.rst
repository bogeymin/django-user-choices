Django User Choices
*******************

:Author: F.S. Davis <consulting@fsdavis.com>

Introduction
============

Django User Choices is a re-usable app for implementing user-defined choices,
instead of the `hard-coded approach built in to Django`_.

The app provides a means to quickly and consistently implement validation
("lookup") models for your types, categories, etc. Users can define their own
lookup values while maintaining data integrity.

.. _hard-coded approach built in to Django: https://docs.djangoproject.com/en/dev/ref/models/fields/#choices

When to Use
-----------

If you have a few choices that need not (or should not) change upon user
preference, Django User Choices is not what you're looking for; use Django's
built-in choices attribute.

This app provides models to facilitate the creation of user-defined choices,
and is most appropriate when your users need control over lookup data.

Installation
============

Installation is easy::

    pip install -e git+https://git@bitbucket.org/bogeymin/django-user-choices.git

Requirements
------------

- Django is the only requirement. The module has been tested with 1.4 but
  should work with older versions of Django.

Usage
=====

Basic Usage
-----------

Basic usage requires only that you extend the lookup class of your choosing::

    from user_choices.models import ALookupString
    class MyCategory(ALookupString):
        pass

However, you will probably want to define your own verbose name::

    class MyCategory(ALookupString):
        class Meta:
            verbose_name = _("Category")
            verbose_name_plural = _("Categories")

Then you simply refer to your extending class in your model::

    class MyProject(models.Model):
        category = models.ForeignKey(MyCategory)
        # ...

Integers for Primary Keys
-------------------------

The example above provides a lookup with a string value, but you can also
create a numeric lookup. For example, if you wanted a priority field::

    from user_choices.models import ALookupInteger
    class MyPriority(ALookupInteger):
        class Meta:
            verbose_name = _("Priority")
            verbose_name_plural = _("Priorities")

    class MyProject(models.Model):
        priority = models.ForeignKey(MyPriority)

Custom Lookup Type
------------------

You could also provide your own lookup type::

    from duser_choices.models import ALookup
    class MyOwnLookup(ALookup):
        value = models.DecimalField(_("value"), primary_key=True)

Admin Example
-------------

You may use the ``LookupAdmin`` class (found in ``extras``) to easily create an
admin interface for any of your lookups, or you can create a completely custom
admin.  A string-based lookup might be administered like so::

    class CategoryAdmin(admin.ModelAdmin):

        # Lists.
        list_display = ('value', 'label', 'label_plural', 'parent', 'sort_order',)
        list_editable = ('label', 'label_plural', 'sort_order',)

        # Forms.
        fieldsets = (
            (None, {
                'fields': ('label', 'value', 'description',)
            }),
            ('Advanced Options', {
                'classes': ['collapse',],
                'fields': ('label_plural', 'parent', 'sort_order',),
                'description': "Additional options improve and refine how lookups can be handled."
            }),
        )
        prepopulated_fields = {"value": ("label",)}

Other Solutions
===============

I created Django User Choices because I couldn't find a generic app to use for
user-defined choices -- something that I implement in almost every Django
project. There are, however, a few related apps and articles that may be of
interest.

- `Django Model Utils`_ provides a convenient and simple tool for defining
  choices programmatically.
- There is also the `Django Choices`_ app which, though perhaps a little
  over-engineered, provides another means of defining choices programmatically.
- James Bennett wrote an article on `removing magic numbers from choices`_ and
  there is also a `follow-up article by ptone.com`_ that provides a script for
  making it easy to implement for TextMate users.

.. _Django Choices: https://github.com/bigjason/django-choices
.. _Django Model Utils: https://github.com/carljm/django-model-utils#choices
.. _follow-up article by ptone.com: http://ptone.com/dablog/2010/05/easy-django-choices-done-right/
.. _removing magic numbers from choices: http://www.b-list.org/weblog/2007/nov/02/handle-choices-right-way/

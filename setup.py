import os
from setuptools import setup

def read(file_path):
    """Shortcut for reading in the long description from a file that is
    relative to the setup script.."""
    return open(os.path.join(os.path.dirname(__file__), file_path)).read()

setup(
    name="django-user-choices",
    version="0.2.1d",
    author="F.S. Davis",
    author_email="consulting@fsdavis.com",
    description = ("A tool for creating and managing lookup data in Django."),
    license="BSD",
    keywords="django database validation foreignkey lookup",
    url="http://packages.python.org/django-user-choices",
    packages=["user_choices", ],
    long_description=read('README.rst'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        'Environment :: Web Environment',
        "Framework :: Django",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        'Operating System :: OS Independent',
        "Programming Language :: Python",
        "Topic :: Database",
    ],
)

"""
Extras for making the user choices app easier to use.
"""

# Python Imports

# Django Imports
from django.contrib import admin

# Local Imports

# Model Admins


class LookupAdmin(admin.ModelAdmin):

    # Lists.
    list_display = ('value', 'label', 'label_plural', 'parent', 'sort_order',)
    list_editable = ('label', 'label_plural', 'sort_order',)

    # Forms.
    fieldsets = (
        (None, {
            'fields': ('label', 'value', 'description',)
        }),
        ('Advanced Options', {
            'classes': ['collapse',],
            'fields': ('label_plural', 'parent', 'sort_order',),
            'description': "Additional options improve and refine how lookups can be handled."
        }),
    )
    prepopulated_fields = {"value": ("label",)}

# Django Imports
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Models


class ALookup(models.Model):
    """Base class for validation data. To extend, add a value field."""
    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("Optional description of the lookup that may be displayed to the user."),
        null=True
    )

    label = models.CharField(
        _("label"),
        help_text=_("Human-friendly label for the value."),
        max_length=128,
        unique=True
    )

    # NOTE: If we make the label_plural unique, Django will complain that it is
    # a duplicate when no input is given. See
    # https://docs.djangoproject.com/en/dev/ref/models/fields/#null
    label_plural = models.CharField(
        _("plural label"),
        blank=True,
        help_text=_("Plural version of the human-friendly label."),
        max_length=128,
        null=True,
    )

    parent = models.ForeignKey('self',
        blank=True,
        null=True
    )

    sort_order = models.IntegerField(
        _("sort order"),
        blank=True,
        help_text=_("Order in which the lookup should be sorted. Default is by label."),
        null=True
    )

    class Meta:
        abstract = True
        ordering = ('sort', 'label',)

    def __unicode__(self):
        return self.plural()

    def plural(self):
        """Return the plural label if it exists, otherwise the label is
        returned.
        """
        # QUESTION: If ``label_plural`` does not exist, should ``label`` be
        # returned with an "s" on the end?
        return self.label_plural or self.label


class ALookupString(ALookup):
    """Base class for char-based validation data."""
    value = models.SlugField(
        _("value"),
        help_text=_("Internal value of the lookup."),
        primary_key=True)

    class Meta:
        abstract = True
        ordering = ['sort_order', 'label']


class ALookupInteger(ALookup):
    """Base class for integer-based validation data."""
    value = models.IntegerField(
        _("value"),
        help_text=_("Internal value of the lookup."),
        primary_key=True)

    class Meta:
        abstract = True
        ordering = ['sort_order', 'value', 'label',]

